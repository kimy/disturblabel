PyTorch implementation of [DisturbLabel: Regularizing CNN on the Loss Layer](https://arxiv.org/abs/1605.00055) [CVPR 2016]

This is a modified version of https://github.com/amirhfarzaneh/disturblabel-pytorch/blob/master/README.md project

- added support of CIFAR10
- added possibility of data augmentation (random cropping and horizontal flipping)


**Most important arguments:**

`--dataset` - which data to use (MNIST or CIFAR10). Default: MNIST

`-- mode` - regularization method applied

Possible values:

| value | method |
| ------ | ------ |
|noreg    |Without any regularization    |
|dl       |Vanilla DistrubLabel          |
|dropout  |Dropout                       |
|dldr     |DistrubLabel+Dropout          |


`--alpha` - alpha for vanilla Distrub label

Possible values: int from 0 to 100. 
Default: 20

`--epochs` - number of training epochs

Default: 100


`--dataaug` - whether data augmentation is applied

Possible values: True, False. Default: False



**Example:**

Running the experiment with Disturb Label with alpha=20,
redirecting the output into file "dl20noaug.log":

`python main.py --mode=dl --alpha=20  > dl20noaug.log`

Command can be generated using UI utility (requires [PyQt5](https://pypi.org/project/PyQt5/)). Usage:

`python constructor.py`


Tensorboard logs will be stored in the folder *./logs/MNIST/LeNet/*

# Results of the runs for CIFAR10, LeNet, without data augmentation (recognition error rate on test set)

| Method   | Our run, best error across all epochs | Our run, error at epoch 100 | Result from baseline paper |
| ------ | ------ | --- | ----| 
| no reg. | 24.67 |24.97 | 22.50 |
| Dropout | 22.82 |22.76 | 19.42 |
| DL alpha=10 | 23.03| 23.15 |20.26|
| Dropout + DL alpha=10 |22.23 |  22.46 |19.18|

![noreg](graphs/LeNet_CIFAR10_noreg.png) 
![dropout](graphs/LeNet_CIFAR10_dropout.png)
![DL](graphs/LeNet_CIFAR10_DL.png) 
![dropoutDL](graphs/LeNet_CIFAR10_dropout_DL.png)


# Results of the runs for MNIST, LeNet, without data augmentation (recognition error rate on test set)

| Method   | Our run, error at epoch 100 | Result from baseline paper |
| ------ | ------ | ----| 
| no reg. |  0.9  | 0.86 |
| Dropout | 0.69  | 0.68 |
| DL alpha=10 | 0.69 | 0.66|
| Dropout + DL alpha=10 |0.67 |  0.63|

![noreg](graphs/LenetMNIST_noreg.png) 
![dropout](graphs/LenetMNIST_Dr.png)
![DL](graphs/LenetMNIST_DL_10.png) 
![dropoutDL](graphs/LenetMNIST_DLDr.png)
