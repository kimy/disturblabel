#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import (QPushButton, QApplication, QDesktopWidget,
                             QMainWindow, QLabel, QLineEdit,
                             QComboBox, QCheckBox)


modes={'No regularization':'noreg',
       'DisturbLabel':'dl',
       'Dropout':'dropout',
       'DisturbLabel+Dropout':'dldr',
#       'DisturbTopConfidentLabels':'dtopc',
#       'DisturbTopConfidentLabels+Dropout':'dtopcdr',
#       'DisturbTopUnconfidentLabels':'dtopu',
#       'DisturbTopUnconfidentLabels+Dropout':'dtopudr',
#       'DisturbConfidentLabels':'dcl',
#       'DisturbConfidentLabels+Dropout':'dcldr',
       'DisturbStochasticConfLabel': 'stochconf',
       'DisturbStochasticConfLabel+Dropout': 'stochconfdr', 
       'DisturbStochasticNonConfLabel': 'stochnonconf', 
       'DisturbStochasticNonConfLabel+Dropout':'stochnonconfdr'}

datasets=['MNIST', 'CIFAR10']
models=['LeNet']
devices=['gpu', 'cpu']
optimizers=['SGD', 'Adam']


defaultText=''


class Generator(QMainWindow):

    def __init__(self):
        super().__init__()

        self.initUI()


    def initUI(self):

        self.resize(1025, 375)
        self.center()
        self.setWindowTitle('Commands Constructor')
        
        self.datasetLabel = QLabel("Dataset", self)
        self.datasetLabel.move(25,25)
        self.dataset = QComboBox(self)
        self.dataset.setGeometry(300, 25, 300, 25)
        self.dataset.addItems(datasets)

        self.modelLabel = QLabel("Model", self)
        self.modelLabel.move(25,50)
        self.model = QComboBox(self)
        self.model.setGeometry(300, 50, 300, 25)
        self.model.addItems(models)
        
        
        self.modeLabel = QLabel("Regularization method", self)
        self.modeLabel.setGeometry(25, 75, 300, 25)
        self.mode = QComboBox(self)
        self.mode.setGeometry(300, 75, 300, 25)
        self.mode.addItems(modes.keys())
        
        self.mode.activated.connect(self.showOptions)
        
        
        self.optionLabel=QLabel('', self)
        self.optionLabel.setGeometry(25, 100, 300, 25)
        self.optionLabel.hide()
        self.option = QLineEdit(self)
        self.option.setGeometry(300, 100, 50, 25)
        self.option.hide()
        self.defOption=QLabel('',self)
        self.defOption.setGeometry(375, 100, 100,25)
        self.defOption.hide()
        
        self.alphaLabel=QLabel('', self)
        self.alphaLabel.setGeometry(25, 125, 300, 25)
        self.alphaLabel.hide()
        self.alpha = QLineEdit(self)
        self.alpha.setGeometry(300, 125, 50, 25)
        self.alpha.hide()
        self.defAlpha=QLabel('',self)
        self.defAlpha.setGeometry(375, 125, 100,25)
        self.defAlpha.hide()
        
    
        
            
        self.dataAugLabel = QLabel("Data augmentation", self)
        self.dataAugLabel.setGeometry(625, 25, 200, 25)
        self.dataAug = QCheckBox('Apply', self)
        self.dataAug.setGeometry(825, 25, 120, 25)
        
        
        self.epochsLabel = QLabel("Total number of epochs", self)
        self.epochsLabel.setGeometry(625, 50, 200, 25)
        self.epochs = QLineEdit(self)
        self.epochs.setGeometry(825, 50, 50, 25)
        self.defEpochs=QLabel('Default: 100',self)
        self.defEpochs.setGeometry(900, 50, 100,25)
        
        
        self.startEpochLabel = QLabel("Start regularizing from epoch", self)
        self.startEpochLabel.setGeometry(625, 75, 200, 25)
        self.startEpoch = QLineEdit(self)
        self.startEpoch.setGeometry(825, 75, 50, 25)
        self.defStartEpochs=QLabel('Default: 10',self)
        self.defStartEpochs.setGeometry(900, 75, 100,25)
        
        self.deviceLabel = QLabel("Device", self)
        self.deviceLabel.setGeometry(625,100,50,25)
        self.device = QComboBox(self)
        self.device.setGeometry(825, 100, 50, 25)
        self.device.addItems(devices)
        
        self.optLabel = QLabel("Optimizer", self)
        self.optLabel.setGeometry(625,125,200,25)
        self.opt = QComboBox(self)
        self.opt.setGeometry(825, 125, 75, 25)
        self.opt.addItems(optimizers)
        


        self.disclaimer = QLabel('Note: empty fields leave values default', self)
        self.disclaimer.setGeometry(25, 175, 300, 25)
        
        
        self.redirectToFile =  QCheckBox('Generate detailed name for the output folder', self)
        self.redirectToFile.setGeometry(25, 200, 400,25)
        self.redirectToFile.toggle()
        self.generatedCommand = QLineEdit(self)
        self.generatedCommand.setText(defaultText)
        self.generatedCommand.setGeometry(25, 225, 975, 50)
        
        self.generate = QPushButton('Generate', self)
        self.generate.setGeometry(25, 300, 475, 50)
        self.generate.clicked.connect(self.changeText)
        
        self.copyToClip = QPushButton('Copy to clipboard', self)
        self.copyToClip.setGeometry(525, 300, 475, 50)
        self.copyToClip.clicked.connect(self.copyText)
    
        
        

        self.show()
        
    def center(self):

        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())
        

    def getValues(self):
        modeVal=modes[self.mode.currentText()]
        dataVal=self.dataset.currentText()
        modelVal=self.model.currentText().lower()
        optVal=self.opt.currentText()
        
        if self.dataAug.isChecked():
            dataAugVal=' --dataaug=True'
            DA='DA'
        else:
            dataAugVal=''
            DA='noDA'

        epochsVal=self.getTextBoxValue(self.epochs, '--epochs')   
        startEpochVal=self.getTextBoxValue(self.startEpoch,'--startreg')
        
        optionVal=self.option.text()
        optionName=''
        if optionVal !='':
            
            if modeVal in ['dtopc', 'dtopcdr', 'dtopu', 'dtopudr']:
                optionName=' --top='
            elif modeVal in ['dcl', 'dcldr','stochconf','stochconfdr', 'stochnonconf', 'stochnonconfdr']:
                optionName=' --conf='
                
        alphaVal=self.alpha.text()
        alphaName=''
        if alphaVal !='':
            if modeVal in ['dl', 'dldr', 'stochconf','stochconfdr', 'stochnonconf', 'stochnonconfdr']:
                alphaName=' --alpha='
            
        
        deviceVal=self.device.currentText()      
        output=self.redirectOutput(dataVal,modelVal,modeVal,optionVal, alphaVal,DA,optVal)
        
        values=' --mode='+modeVal+ \
                ' --dataset='+dataVal+ \
                ' --opt='+optVal+ \
                dataAugVal+ \
                epochsVal+startEpochVal+ \
                optionName+optionVal+ \
                alphaName+alphaVal+\
                ' --device='+deviceVal+ \
                output
        return (values)
        
    def redirectOutput(self,data,model,mode,option,alpha,da,opt):
        if self.redirectToFile.isChecked():
            fileName='  --logf='+data+'_'+model+'_'+mode+'_'+option+'_'+alpha+'_'+da+'_'+opt+'_startep_'+self.startEpoch.text()
        else:
            fileName=''
        
        return fileName
    
            
    def getTextBoxValue(self,objectName,label):
        if objectName.text()=='':
            textBoxValue=''
        else:
            textBoxValue=' '+label+'='+objectName.text()
        
        return textBoxValue
            
    def changeText(self):
        text=self.getValues()
        self.generatedCommand.setText(defaultText+text)
        
    def copyText(self):
        self.generatedCommand.selectAll()
        self.generatedCommand.copy()
        
    def showOptions(self):
        selectedMode=modes[self.mode.currentText()]
        self.option.clear()
        self.alpha.clear()
        
        if selectedMode in ['dl', 'dldr']:
            self.alphaLabel.setText('Alpha (integer in range 0-100)')
            self.defAlpha.setText('Default: 20')
            self.alphaLabel.show()
            self.alpha.show()
            self.defAlpha.show()
        elif selectedMode in ['dtopc', 'dtopcdr', 'dtopu', 'dtopudr']:
            self.optionLabel.setText('Top (integer in range 0-100)')
            self.defOption.setText('Default: 30')
            self.optionLabel.show()
            self.option.show()
            self.defOption.show()
            self.alphaLabel.hide()
            self.alpha.hide()
            self.defAlpha.hide()
            
        elif selectedMode in ['dcl', 'dcldr']:
            self.optionLabel.setText('Confidence threshold (float in range 0-1)')
            self.defOption.setText('Default: 0.8')
            self.optionLabel.show()
            self.option.show()
            self.defOption.show()
            self.alphaLabel.hide()
            self.alpha.hide()
            self.defAlpha.hide()
            
        elif selectedMode in ['stochconf','stochconfdr', 'stochnonconf', 'stochnonconfdr']:
            self.alphaLabel.setText('Alpha (integer in range 0-100)')
            self.defAlpha.setText('Default: 20')
            self.alphaLabel.show()
            self.alpha.show()
            self.defAlpha.show()
            self.optionLabel.setText('Confidence threshold (float in range 0-1)')
            self.defOption.setText('Default: 0.8')
            self.optionLabel.show()
            self.option.show()
            self.defOption.show()
            
        elif selectedMode in ['noreg', 'dropout']:
            self.optionLabel.hide()
            self.option.hide()
            self.defOption.hide()
            self.alphaLabel.hide()
            self.alpha.hide()
            self.defAlpha.hide()




def main():

    app = QApplication(sys.argv)
    ex = Generator()
    sys.exit(app.exec_())    


if __name__ == '__main__':
    main()