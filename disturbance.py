import torch
import torch.nn.functional as F
   

class DisturbLabel(torch.nn.Module):
    def __init__(self, alpha, C):
        super(DisturbLabel, self).__init__()
        self.alpha = alpha
        self.C = C
        # Multinoulli distribution
        self.p_c = (1 - ((C - 1)/C) * (alpha/100))
        self.p_i = (1 / C) * (alpha / 100)

    def forward(self, y):
        
        if y.shape[0]==0:
            y_disturbed=y
        else:           
            # convert classes to index
            y_tensor = y
            y_tensor = y_tensor.type(torch.LongTensor).view(-1, 1)
    
            # create disturbed labels
            depth = self.C
            y_one_hot = torch.ones(y_tensor.size()[0], depth) * self.p_i
            y_one_hot.scatter_(1, y_tensor, self.p_c)
            y_one_hot = y_one_hot.view(*(tuple(y.shape) + (-1,)))
    
            # sample from Multinoulli distribution
            distribution = torch.distributions.OneHotCategorical(y_one_hot)
            y_disturbed = distribution.sample()
            y_disturbed = y_disturbed.max(dim=1)[1]  # back to categorical

        return y_disturbed


class DisturbConfident(torch.nn.Module):
    def __init__(self, confidence, disturb, mode, device):
        super(DisturbConfident, self).__init__()
        self.conf = confidence
        self.disturb = disturb
        self.mode=mode
        self.device=device

        
    def forward(self, output, target):
        outputs_prob=F.softmax(output.float(),dim=1)
                           
        max_prob=torch.max(outputs_prob, 1)[0]
        
        confident_output=output[max_prob > self.conf]
        nonconfident_output=output[max_prob <=self.conf]
        
        confident_targets=target[max_prob > self.conf]
        nonconfident_targets = target[max_prob <= self.conf]
        
        if self.mode in ['stochconf', 'stochconfdr']:
            targets_disturbed=self.disturb(confident_targets).to(self.device)
            targets_original=nonconfident_targets.to(self.device)
            
            output_disturbed = confident_output.to(self.device)
            output_original = nonconfident_output.to(self.device)
            
            
        elif self.mode in ['stochnonconf', 'stochnonconfdr']: 
            targets_disturbed = self.disturb(nonconfident_targets).to(self.device)
            targets_original = confident_targets.to(self.device)
            
            output_disturbed = nonconfident_output.to(self.device)
            output_original = confident_output.to(self.device)                 
        
        num_disturbed=targets_disturbed.shape[0]
        target=torch.cat((targets_disturbed, targets_original), dim=0)
        output=torch.cat((output_disturbed, output_original), dim=0)
                               
        return output.to(self.device), target.to(self.device), num_disturbed