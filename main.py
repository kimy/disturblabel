import os
import argparse
import time

import torch
import torch.nn as nn
#import torch.nn.functional as F

from LeNetModels import LeNetC, LeNetM, LeNetMAug
from BigNetModels import BigNet
from dataloaderCIFAR10 import dataCIFAR10
from dataloaderMNIST import dataMNIST
from disturbance import DisturbLabel, DisturbConfident
#from lcn import local_contrast_norm as lcn

from tensorboardX import SummaryWriter
import numpy as np

def main():
    # parameters
    parser = argparse.ArgumentParser(description='PyTorch DisturbLabel')
    parser.add_argument('--mode', type=str, default='dldr')
    parser.add_argument('--alpha', type=int, default=20)
    parser.add_argument('--batch-size', type=int, default=32)
    parser.add_argument('--test-batch-size', type=int, default=32)
    parser.add_argument('--epochs', type=int, default=100)
    parser.add_argument('--lr', type=float, default=0.001)
    parser.add_argument('--momentum', type=float, default=0.9)
    parser.add_argument('--device', type=str, default='gpu')
    parser.add_argument('--num-workers', type=int, default=4)
    parser.add_argument('--dataaug', type=bool, default=False)
    parser.add_argument('--model', type=str, default='lenet')
    parser.add_argument('--dataset', type=str, default='MNIST')
    parser.add_argument('--startreg', type=int, default=10)
    parser.add_argument('--conf', type=float, default=0.8)
    parser.add_argument('--logf', type=str, default='def')
    parser.add_argument('--init', type=str, default='def') #xavier_uniform, xavier_normal
    parser.add_argument('--optim', type=str, default='SGD') #SGD, Adam
    parser.add_argument('--lcn', type=bool, default=False)

    args = parser.parse_args()


    global writer
    mode=args.mode
    LOGS_PATH='logs'+'/'+str(args.dataset)+'/'+str(args.model)

    if args.logf=='def':
        if mode=='dl' or mode=='dldr':
            FOLDER_NAME=mode+'_alpha_'+str(args.alpha)
    
#        elif mode=='dtopc' or mode=='dtopu' or mode=='dtopcdr' or mode=='dtopudr':
#            FOLDER_NAME=mode+'_top_'+str(args.top)
   
        elif mode=='stochconf' or mode=='stochconfdr':
            FOLDER_NAME=mode+'_conf_'+str(args.conf)+'_alpha_'+str(args.alpha)
    
        else:
            FOLDER_NAME=mode
    
    else:
        FOLDER_NAME=args.logf
    

    writer = SummaryWriter(os.path.join(LOGS_PATH, FOLDER_NAME, 'tb'))
    # GPU/CPU
    device = torch.device('cuda' if args.device == 'gpu' else 'cpu')
#%%
    # Reading data
    
    if args.dataset=='CIFAR10':
        dataset=dataCIFAR10(args.dataaug, args.batch_size, args.test_batch_size, args.num_workers)
    elif args.dataset=='MNIST':
        dataset=dataMNIST(args.dataaug, args.batch_size, args.test_batch_size, args.num_workers)
        
     
    train_loader, test_loader=dataset.loadData()
    
#    if args.dataset=='CIFAR10' and args.lcn==True:
#        data = lcn(data, radius=9)
        

#%%
    # Model
    if mode in ['dropout', 'dldr', 'stochconfdr', 'stochnonconfdr']:
        use_dropout=True
    else:
        use_dropout=False
        
    milestones=[40, 60, 80] 
    
    if args.model=='lenet':
        milestones=[40, 60, 80]
        if args.dataset =='CIFAR10':
            model = LeNetC(use_dropout).to(device)
           
        elif args.dataset =='MNIST':
            if args.dataaug==True:
                model =  LeNetMAug(use_dropout).to(device)
            else:
                model = LeNetM(use_dropout).to(device)

    if args.model=='bignet' and args.dataset =='CIFAR10':
        milestones=[200, 300, 400]
        model=BigNet(use_dropout).to(device)
        print('BigNet')

#%% Optimizer + scheduler
    if args.optim=='SGD':
        optimizer = torch.optim.SGD(model.parameters(), lr=args.lr, momentum=args.momentum)
    elif args.optim=='Adam':
        optimizer = torch.optim.Adam(model.parameters(), lr=args.lr)
    
    scheduler=torch.optim.lr_scheduler.MultiStepLR(optimizer, milestones=milestones, gamma=0.1)
        

#%% Weight initialization
    def weights_init(m):
        if isinstance(m, nn.Conv2d) or isinstance(m, nn.Linear):
            if args.init=='xavier_uniform':
                torch.nn.init.xavier_uniform_(m.weight)
            elif args.init=='xavier_normal':
                torch.nn.init.xavier_normal_(m.weight)


    if args.init !='def':
        model.apply(weights_init)
        
#%%
    criterion = nn.CrossEntropyLoss().to(device)
    disturb = None
    disturbConf = None

    if mode in ['dl','dldr','stochconf', 'stochconfdr','stochnonconf', 'stochnonconfdr']:
        disturb = DisturbLabel(alpha=args.alpha, C=10)
        
    if mode in ['stochconf', 'stochconfdr', 'stochnonconf', 'stochnonconfdr']:   
        disturbConf = DisturbConfident(confidence=args.conf, disturb=disturb, mode=mode, device=device)

#    if mode == 'dtopc' or mode=='dtopcdr' or mode=='dtopudr' or mode == 'dtopu' or args.mode == 'dcl' or args.mode == 'dcldr':
#        disturb=DisturbLabel(C=10)
        
#%%
    # Train and Test
    start=time.time()
    output_dump=np.zeros((args.epochs, 5))

    
    for epoch in range(1, args.epochs + 1):
        epoch_start=time.time()
        train_error, train_loss, num_disturbed=train(args, model, device, train_loader, optimizer, criterion, epoch, disturb, disturbConf)
        test_error, test_loss=test(args, model, device, test_loader, criterion, epoch)
        scheduler.step()
        output_dump[epoch-1,0]=train_error
        output_dump[epoch-1,1]=test_error
        output_dump[epoch-1,2]=train_loss
        output_dump[epoch-1,3]=test_loss
        output_dump[epoch-1,4]=num_disturbed
        
        writer.add_scalars('{0}/loss'.format(args.mode), {'train':train_loss,
                                    'test':test_loss}, epoch)
        writer.add_scalars('{0}/error'.format(args.mode), {'train':train_error,
                                    'test':test_error}, epoch)
        print('Elapsed: {0:.2f} seconds for epoch, {1:.2f} minutes in total\n'.format(
                                            time.time()-epoch_start, (time.time()-start)/60))
    
    print("Smallest Train Error={0:.2f}% is achieved at epoch {1}".format(np.min(output_dump[:,0], axis=0), np.argmin(output_dump[:,0], axis=0)))
    print("Smallest Test Error={0:.2f}% is achieved at epoch {1}".format(np.min(output_dump[:,1], axis=0), np.argmin(output_dump[:,1], axis=0)))
    np.save((os.path.join(LOGS_PATH, FOLDER_NAME)+'/ErrAndLoss'), output_dump)
    print('Log folder: {0}'.format(LOGS_PATH))
    print('Log subfolder: {0}'.format(FOLDER_NAME))
    print('Error and Loss logs are saved to ErrAndLoss.npy file')
    
#%%
def train(args, model, device, train_loader, optimizer, criterion, epoch, disturb, disturbConf):
    model.train()
    correct = 0
    running_loss = 0.0
    disturbed_count = 0.0
    
    for batch_idx, (data, target) in enumerate(train_loader, 0):
        data, target = data.to(device), target.to(device)
        
#        if args.dataset =='CIFAR10':
#                data = lcn(data, radius=9)
        
        # disturb labels
        if args.mode == 'dl' or args.mode == 'dldr':
            target = disturb(target).to(device)
                   
        optimizer.zero_grad()
        output = model(data)
       
        if args.mode  in ['stochconf', 'stochconfdr', 'stochnonconfdr', 'stochnonconf']:
            if epoch > args.startreg:
                
                output, target, dist_in_batch = disturbConf(output, target)
                disturbed_count += dist_in_batch                            
      
        loss = criterion(output, target)
        loss.backward()
        optimizer.step()
        running_loss += loss.item() 
        

        # calculate error rate
        pred = output.argmax(dim=1, keepdim=True)
        correct += pred.eq(target.view_as(pred)).sum().item()
        

    train_error = 100 - (100. * correct / len(train_loader.dataset))
    train_loss=running_loss/(batch_idx+1)
    share_disturbed= disturbed_count*100 / len(train_loader.dataset)
    print('Epoch [{0}] Train Loss: {1:.4f} | Error: {2:.2f}% '.format(epoch, train_loss, train_error))
    if args.mode  in ['stochconf', 'stochconfdr', 'stochnonconfdr', 'stochnonconf']:
        print('Share of labels disturbed: {0:.2f}% '.format(share_disturbed))
    return train_error, train_loss, share_disturbed


def test(args, model, device, test_loader, criterion, epoch):
    model.eval() #eval mode switches off applying dropout
    correct = 0
    running_test_loss = 0.0
    
    with torch.no_grad(): #no_grad prevents gradients calculation
          
        for batch_idx, (data, target) in enumerate(test_loader, 0):
            data, target = data.to(device), target.to(device)
#            if args.dataset =='CIFAR10':
#                data = lcn(data, radius=9)
            output = model(data)
            loss = criterion(output, target)


            pred = output.argmax(dim=1, keepdim=True)
            correct += pred.eq(target.view_as(pred)).sum().item()
            running_test_loss+=loss.item()

    test_error = 100 - (100. * correct / len(test_loader.dataset))
    test_loss=running_test_loss/(batch_idx+1)
    print('Epoch [{0}] Test Loss: {1:.4f} | Error: {2:.2f}%'.format(epoch, test_loss, test_error))
        
    return test_error, test_loss


if __name__ == '__main__':
    main()

