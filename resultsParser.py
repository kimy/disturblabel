import os
import numpy as np
import matplotlib.pyplot as plt


'''Enter parameters before running:'''
LOGS_PATH ='./logs/410/logs/CIFAR10/lenet' #add here path to Log folder
FOLDER_NAME='CIFAR10_lenet_stochconf_0.8_50_noDA_SGD_startep_5' #add here path to Log subfolder
epochs=100 #number of epochs

data=np.load(os.path.join(LOGS_PATH, FOLDER_NAME)+'/'+'ErrAndLoss.npy')
graph_title='CIFAR10_lenet_stochconf_0.8_50_noDA_SGD_startep_5' #Enter the desired title for the graph
#%%
def graph(log, title):
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(10, 3))
    fig.suptitle(title)

    ax1.plot(range(epochs), log[:,0], label='train error', c='b')
    ax1.plot(range(epochs), log[:,1], label='test error', c='r')
    ax1.set_xlabel('Epoch')
    ax1.set_ylabel('Error')
#    ax1.set_ylim([0,100]) #adjust limit if needed
    ax1.legend()
    
    ax2.plot(range(epochs), log[:,2], label='train loss', c='b')
    ax2.plot(range(epochs), log[:,3], label='test loss', c='r')
    ax2.set_xlabel('Epoch')
    ax2.set_ylabel('Loss')
#    ax2.set_ylim([0,1])
    ax2.legend()
    
    fig.show()
    fig.savefig(os.path.join(LOGS_PATH, FOLDER_NAME)+'/'+title+'.png')
    
def results(log):
    print ('Across all epochs:', round(np.min(log[:,1], axis=0),2),'last epoch:', round(log[epochs-1,1],2))
    
def plot_results(title): 
    graph(data, title)
    results(data)
    
def graph_disturbed(title):
    plt.plot(range(epochs), data[:,4], label='Share of disturbed labels', c='g')
    plt.title(title)
    plt.xlabel('Epoch')
    plt.ylabel('Share')
    plt.ylim([0,100])
    plt.legend()
    plt.show()
    plt.savefig(os.path.join(LOGS_PATH, FOLDER_NAME)+'/shDist_'+title+'.png')
    
#%%
plot_results(graph_title) 
#%%
graph_disturbed(graph_title)
